class ClientFormController{
    constructor(ToastService,$state,API,$log){
        'ngInject';
        this.ToastService = ToastService;
        this.$state = $state;
        this.API = API;
        this.$log = $log;
    }

    $onInit(){
    }
    submit(){
        let record = {
            name:this.name,
            email:this.email,
            phone:this.phone,
            location:this.location,
            type:this.type
        };
        this.API.all('clients').post(record)
        .then((response)=>{
            this.$log.log(response);
        }).catch(this.failedSubmission.bind(this));
    }
    failedSubmission(response) {
        if (response.status === 422) {
            for (let error in response.data.errors) {
                return this.ToastService.error(response.data.errors[error][0]);
            }
        }
        this.ToastService.error(response.statusText);
    }
}

export const ClientFormComponent = {
    templateUrl: './views/app/components/client-form/client-form.component.html',
    controller: ClientFormController,
    controllerAs: 'vm',
    bindings: {}
}
