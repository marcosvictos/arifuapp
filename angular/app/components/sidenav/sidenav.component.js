class SidenavController{
    constructor($mdSidenav){
        'ngInject';
        this.$mdSidenav = $mdSidenav;

        //
    }

    $onInit(){
    }
    
    toggleLeftMenu(){
        this.$mdSidenav('left').toggle();
    }
}

export const SidenavComponent = {
    templateUrl: './views/app/components/sidenav/sidenav.component.html',
    controller: SidenavController,
    controllerAs: 'vm',
    bindings: {}
}
