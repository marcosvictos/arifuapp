class ClientsController{
    constructor(){
        'ngInject';

        //
    }

    $onInit(){
    }
}

export const ClientsComponent = {
    templateUrl: './views/app/components/clients/clients.component.html',
    controller: ClientsController,
    controllerAs: 'vm',
    bindings: {}
}
