class LandingController{
    constructor(){
        'ngInject';

        //
    }

    $onInit(){
    }
}

export const LandingComponent = {
    templateUrl: './views/app/components/landing/landing.component.html',
    controller: LandingController,
    controllerAs: 'vm',
    bindings: {}
}
