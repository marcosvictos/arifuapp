import {ClientsComponent} from './app/components/clients/clients.component';
import {ClientFormComponent} from './app/components/client-form/client-form.component';
import {LandingComponent} from './app/components/landing/landing.component';
import {SidenavComponent} from './app/components/sidenav/sidenav.component';
import {ResetPasswordComponent} from './app/components/reset-password/reset-password.component';
import {ForgotPasswordComponent} from './app/components/forgot-password/forgot-password.component';
import {LoginFormComponent} from './app/components/login-form/login-form.component';
import {RegisterFormComponent} from './app/components/register-form/register-form.component';

angular.module('app.components')
	.component('clients', ClientsComponent)
	.component('clientForm', ClientFormComponent)
	.component('landing', LandingComponent)
	.component('sidenav', SidenavComponent)
	.component('resetPassword', ResetPasswordComponent)
	.component('forgotPassword', ForgotPasswordComponent)
	.component('loginForm', LoginFormComponent)
	.component('registerForm', RegisterFormComponent);

