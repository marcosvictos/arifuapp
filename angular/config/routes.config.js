export function RoutesConfig($stateProvider, $urlRouterProvider) {
	'ngInject';

	let getView = (viewName) => {
		return `./views/app/pages/${viewName}/${viewName}.page.html`;
	};

	$urlRouterProvider.otherwise('/');

	$stateProvider
		.state('app', {
			abstract: true,
            data: {},//{auth: true} would require JWT auth
			views: {
				header: {
					templateUrl: getView('header')
				},
				footer: {
					templateUrl: getView('footer')
				},
                sidenav: {
                    templateUrl: getView('sidenav')
                },
				main: {}
			}
		})
		.state('app.landing', {
            url: '/',
            views: {
                'main@': {
                    templateUrl: getView('landing')
                }
            }
        })
        .state('app.login', {
			url: '/login',
			views: {
				'main@': {
					templateUrl: getView('login')
				}
			}
		})
        .state('app.register', {
            url: '/register',
            views: {
                'main@': {
                    templateUrl: getView('register')
                }
            }
        })
        .state('app.forgot_password', {
            url: '/forgot-password',
            views: {
                'main@': {
                    templateUrl: getView('forgot-password')
                }
            }
        })
        .state('app.reset_password', {
            url: '/reset-password/:email/:token',
            views: {
                'main@': {
                    templateUrl: getView('reset-password')
                }
            }
        })
        .state('app.clients', {
            url: '/clients',
            views: {
                'main@': {
                    templateUrl: getView('clients')
                }
            }
        })
        .state('app.create-client', {
            url: '/clients/create',
            views: {
                'main@': {
                    templateUrl: getView('create-client')
                }
            }
        })
        .state('app.users', {
            url: '/users',
            views: {
                'main@': {
                    templateUrl: getView('users')
                }
            }
        })
        .state('app.programs', {
            url: '/programs',
            views: {
                'main@': {
                    templateUrl: getView('programs')
                }
            }
        })
        ;
}
