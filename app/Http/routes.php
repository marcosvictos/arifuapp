<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middleware' => ['web']], function () {

    Route::get('/', 'AngularController@serveApp');

    Route::get('/unsupported-browser', 'AngularController@unsupported');

});

//public API routes
$api->group(['middleware' => ['api']], function ($api) {

    // Authentication Routes...
    $api->post('auth/login', 'Auth\AuthController@login');
    $api->post('auth/register', 'Auth\AuthController@register');

    // Password Reset Routes...
    $api->post('auth/password/email', 'Auth\PasswordResetController@sendResetLinkEmail');
    $api->get('auth/password/verify', 'Auth\PasswordResetController@verify');
    $api->post('auth/password/reset', 'Auth\PasswordResetController@reset');

    // Clients Api
    $api->get('clients', 'ClientController@index');
    $api->post('clients', 'ClientController@create');
    $api->put('clients/{id}', 'ClientController@update');
    $api->delete('clients/{id}', 'ClientController@delete');

    // Programs API
    $api->get('programs', 'ProgramController@index');
    $api->post('programs', 'ProgramController@create');
    $api->put('programs/{id}', 'ProgramController@update');
    $api->delete('programs/{id}', 'ProgramController@delete');

    // Contents API
    $api->get('contents', 'ContentController@index');
    $api->post('contents', 'ContentController@create');
    $api->put('contents/{id}', 'ContentController@update');
    $api->delete('contents/{id}', 'ContentController@delete');

    // Roles API
    $api->get('roles', 'RoleController@index');
    $api->post('roles', 'RoleController@create');
    $api->put('roles/{id}', 'RoleController@update');
    $api->delete('roles/{id}', 'RoleController@delete');

     // Permissions API
    $api->get('permissions', 'PermissionController@index');
    $api->post('permissions', 'PermissionController@create');
    $api->put('permissions/{id}', 'PermissionController@update');
    $api->delete('permissions/{id}', 'PermissionController@delete');
});

//protected API routes with JWT (must be logged in)
$api->group(['middleware' => ['api', 'api.auth']], function ($api) {

});
