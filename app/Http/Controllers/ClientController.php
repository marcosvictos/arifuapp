<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// use App\Http\Requests;

use App\Client;

class ClientController extends Controller
{
    //View All Clients
    public function index(){
    	$clients = Client::all();
    	return response()->success(compact('clients'));
    }
    // Add a new Client
    public function create(Request $request){
    	$this->validate($request, [
	        'name'  => 'required',
	        'email' => 'required',
	        'phone' => 'required',
	        'location' => 'required',
	        'type' => 'required',
	        ]);
    	// $client  = new Client;
        $input = $request->all();
    	$input['visibility'] = 1;
        $input['created_by'] = 1;
    	
    	Client::create($input);

    	return response()->success(compact('input'));

    }
    // Update A client
    public function update($id,Request $request){
    	$client = Client::findOrFail($id);
        $this->validate($request, [
            'name'  => 'required',
            'email' => 'required',
            'phone' => 'required',
            'location' => 'required',
            'type' => 'required',
        ]);
        $update = $request->all();
        $client->fill($update)->save();
    	return response()->success(compact('update'));
    }
    // Delete A client
    public function delete($id){

    	$delete = Client::findOrFail($id);
    	$delete->visibility = false;
        $delete->save();
        return response()->success(compact('delete'));
    }
}
