<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Role;

class RoleController extends Controller
{
    //
    //View All Roles
    public function index(){
    	$roles = Role::all();
    	return response()->success(compact('roles'));
    }
    // Add a new role
    public function create(Request $request){
    	$this->validate($request, [
	        'title'  => 'required',
	        'description' => 'required',
	        
	        ]);
        $input = $request->all();
    	$input['visibility'] = 1;
        $input['created_by'] = 1;
    	
    	Role::create($input);

    	return response()->success(compact('input'));

    }
    // Update A role
    public function update($id,Request $request){
    	$role = Role::findOrFail($id);
        /*$this->validate($request, [
            'code'  => 'required',
            'title' => 'required'
        ]);*/
        $update = $request->all();
        $role->fill($update)->save();
    	return response()->success(compact('update'));
    }
    // Delete A role
    public function delete($id){

    	$delete = Role::findOrFail($id);
    	$delete->visibility = false;
        $delete->save();
        return response()->success(compact('delete'));
    }
}
