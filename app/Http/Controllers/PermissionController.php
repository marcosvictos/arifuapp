<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Permission;

class PermissionController extends Controller
{
    //View All permissions
    public function index(){
    	$permissions = Permission::all();
    	return response()->success(compact('permissions'));
    }
    // Add a new permission
    public function create(Request $request){
    	$this->validate($request, [
	        'title'  => 'required',
	        'description' => 'required',
	        
	        ]);
        $input = $request->all();
    	$input['visibility'] = 1;
        $input['created_by'] = 1;
    	
    	Permission::create($input);

    	return response()->success(compact('input'));

    }
    // Update A permission
    public function update($id,Request $request){
    	$permission = Permission::findOrFail($id);
        /*$this->validate($request, [
            'code'  => 'required',
            'title' => 'required'
        ]);*/
        $update = $request->all();
        $permission->fill($update)->save();
    	return response()->success(compact('update'));
    }
    // Delete A permission
    public function delete($id){

    	$delete = Permission::findOrFail($id);
    	$delete->visibility = false;
        $delete->save();
        return response()->success(compact('delete'));
    }
}
