<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Programs;

class ProgramController extends Controller
{
    //
    //View All Programs
    public function index(){
    	$programs = Programs::all();
    	return response()->success(compact('programs'));
    }
    // Add a new Program
    public function create(Request $request){
    	$this->validate($request, [
	        'code'  => 'required',
	        'title' => 'required',
	        
	        ]);
        $input = $request->all();
    	$input['visibility'] = 1;
        $input['created_by'] = 1;
    	
    	Programs::create($input);

    	return response()->success(compact('input'));

    }
    // Update A program
    public function update($id,Request $request){
    	$program = Programs::findOrFail($id);
        /*$this->validate($request, [
            'code'  => 'required',
            'title' => 'required'
        ]);*/
        $update = $request->all();
        $program->fill($update)->save();
    	return response()->success(compact('update'));
    }
    // Delete A program
    public function delete($id){

    	$delete = Programs::findOrFail($id);
    	$delete->visibility = false;
        $delete->save();
        return response()->success(compact('delete'));
    }
}
