<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Content;

class ContentController extends Controller
{
    //
    //View All Contents
    public function index(){
    	$contents = Content::all();
    	return response()->success(compact('contents'));
    }
    // Add a new Content
    public function create(Request $request){
    	$this->validate($request, [
	        'program'  => 'required',
	        'content' => 'required',
	        
	        ]);
        $input = $request->all();
    	$input['visibility'] = 1;
        $input['created_by'] = 1;
    	
    	Content::create($input);

    	return response()->success(compact('input'));

    }
    // Update A content
    public function update($id,Request $request){
    	$content = Content::findOrFail($id);
        /*$this->validate($request, [
            'code'  => 'required',
            'title' => 'required'
        ]);*/
        $update = $request->all();
        $content->fill($update)->save();
    	return response()->success(compact('update'));
    }
    // Delete A content
    public function delete($id){

    	$delete = Content::findOrFail($id);
    	$delete->visibility = false;
        $delete->save();
        return response()->success(compact('delete'));
    }
}
