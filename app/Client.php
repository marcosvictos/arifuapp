<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    //
    protected $fillable = ['name','email','phone','location','type','visibility','created_by'];
    
}
