<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Programs extends Model
{
    //
    protected $fillable = ['code','title','visibility','created_by'];
}
