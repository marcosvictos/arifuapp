(function(module) {
try {
  module = angular.module('app.partials');
} catch (e) {
  module = angular.module('app.partials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('./views/app/components/client-form/client-form.component.html',
    '<form ng-submit="vm.submit()" flex="80">\n' +
    '	<div layout="row">\n' +
    '		<md-input-container class="md-block" flex="50">\n' +
    '	        <label>Name</label>\n' +
    '	        <input  ng-model="vm.name">\n' +
    '	     </md-input-container>\n' +
    '	     <md-input-container class="md-block" flex="50">\n' +
    '	        <label>Email</label>\n' +
    '	        <input  ng-model="vm.email">\n' +
    '	     </md-input-container>\n' +
    '	</div>\n' +
    '\n' +
    '	<div layout="row">\n' +
    '		<md-input-container class="md-block" flex="50">\n' +
    '	        <label>Phone</label>\n' +
    '	        <input  ng-model="vm.phone">\n' +
    '	     </md-input-container>\n' +
    '	     <md-input-container class="md-block" flex="50">\n' +
    '	        <label>Location</label>\n' +
    '	        <input  ng-model="vm.location">\n' +
    '	     </md-input-container>\n' +
    '	</div>\n' +
    '	<div layout="row">\n' +
    '		<md-input-container class="md-block" flex="50">\n' +
    '	        <label>Type</label>\n' +
    '	       <md-select ng-model="vm.type">\n' +
    '	          <md-option  value="personal">\n' +
    '	            Personal\n' +
    '	          </md-option>\n' +
    '	          <md-option  value="corporate">\n' +
    '	            Corporate\n' +
    '	          </md-option>\n' +
    '	        </md-select>\n' +
    '	     </md-input-container>\n' +
    '	</div>\n' +
    '\n' +
    '	<div layout="row">\n' +
    '		<md-input-container class="md-block">\n' +
    '	        <md-button type="submit" class="md-raised md-primary">Submit</md-button>\n' +
    '	     </md-input-container>\n' +
    '	</div>\n' +
    '	\n' +
    '</form>');
}]);
})();

(function(module) {
try {
  module = angular.module('app.partials');
} catch (e) {
  module = angular.module('app.partials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('./views/app/components/clients/clients.component.html',
    '<md-content>\n' +
    '	<md-button class="md-raised" ui-sref="app.create-client">Add Client</md-button>\n' +
    '</md-content>');
}]);
})();

(function(module) {
try {
  module = angular.module('app.partials');
} catch (e) {
  module = angular.module('app.partials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('./views/app/components/forgot-password/forgot-password.component.html',
    '<form ng-submit="vm.submit()" class="ForgotPassword-form">\n' +
    '    <div>\n' +
    '        <md-input-container>\n' +
    '            <label>Email</label>\n' +
    '            <input type="email" ng-model="vm.email">\n' +
    '        </md-input-container>\n' +
    '\n' +
    '        <md-button type="submit" class="md-primary md-raised">Submit</md-button>\n' +
    '    </div>\n' +
    '</form>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.partials');
} catch (e) {
  module = angular.module('app.partials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('./views/app/components/landing/landing.component.html',
    '   <div layout="column">\n' +
    '   		<md-toolbar md-scroll-shrink>\n' +
    '		    <div class="md-toolbar-tools">{{vm.userType}} Dashboard</div>\n' +
    '		</md-toolbar>\n' +
    '	   <md-content>\n' +
    '			Hi! I\'m Arifu :-)		     \n' +
    '	   </md-content>\n' +
    '   </div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.partials');
} catch (e) {
  module = angular.module('app.partials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('./views/app/components/login-form/login-form.component.html',
    '<form ng-submit="vm.login(login-form.$valid)" name="login-form" novalidate>\n' +
    '	<div>\n' +
    '		<md-input-container class="LoginForm-inputContainer">\n' +
    '			<label>Email</label>\n' +
    '			<input type="email" ng-model="vm.email" name="email">\n' +
    '			<p style="color:red">{{vm._errors.email[0]}}</p>\n' +
    '		</md-input-container>\n' +
    '	</div>\n' +
    '\n' +
    '	<div>\n' +
    '		<md-input-container class="LoginForm-inputContainer">\n' +
    '			<label>Password</label>\n' +
    '			<input type="password" ng-model="vm.password" name="password">\n' +
    '			<p ng-show="login-form.password.$pending ">Checking Password ....</p>\n' +
    '\n' +
    '		</md-input-container>\n' +
    '	</div>\n' +
    '\n' +
    '	<md-button type="submit" class="LoginForm-submit md-primary md-raised">Log in</md-button>\n' +
    '</form>\n' +
    '\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.partials');
} catch (e) {
  module = angular.module('app.partials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('./views/app/components/register-form/register-form.component.html',
    '<form ng-submit="vm.register()">\n' +
    '	<div>\n' +
    '		<md-input-container class="RegisterForm-inputContainer">\n' +
    '			<label>Name</label>\n' +
    '			<input type="text" ng-model="vm.name">\n' +
    '		</md-input-container>\n' +
    '	</div>\n' +
    '\n' +
    '	<div>\n' +
    '		<md-input-container class="RegisterForm-inputContainer">\n' +
    '			<label>Email</label>\n' +
    '			<input type="email" ng-model="vm.email">\n' +
    '		</md-input-container>\n' +
    '	</div>\n' +
    '	<div>\n' +
    '		<md-input-container class="RegisterForm-inputContainer">\n' +
    '			<label>Phone</label>\n' +
    '			<input type="email" ng-model="vm.phone">\n' +
    '		</md-input-container>\n' +
    '	</div>\n' +
    '	<div>\n' +
    '		<md-input-container class="RegisterForm-inputContainer">\n' +
    '			<label>Location</label>\n' +
    '			<input type="email" ng-model="vm.location">\n' +
    '		</md-input-container>\n' +
    '	</div>\n' +
    '	<div>\n' +
    '		<md-input-container class="RegisterForm-inputContainer">\n' +
    '			<label>Password</label>\n' +
    '			<input type="password" ng-model="vm.password">\n' +
    '		</md-input-container>\n' +
    '	</div>\n' +
    '	<div>\n' +
    '		<md-input-container class="RegisterForm-inputContainer">\n' +
    '			<label> Confirm Password</label>\n' +
    '			<input type="password" ng-model="vm.confirm_password">\n' +
    '		</md-input-container>\n' +
    '	</div>\n' +
    '\n' +
    '	<md-button type="submit" class="RegisterForm-submit md-primary md-raised">Register</md-button>\n' +
    '</form>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.partials');
} catch (e) {
  module = angular.module('app.partials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('./views/app/components/reset-password/reset-password.component.html',
    '<form ng-submit="vm.submit()">\n' +
    '\n' +
    '    <div ng-if="!vm.isValidToken" layout="row" layout-align="center center">\n' +
    '        <md-progress-circular md-mode="indeterminate"></md-progress-circular>\n' +
    '    </div>\n' +
    '\n' +
    '    <div ng-show="vm.isValidToken">\n' +
    '        <md-input-container class="ResetPassword-input">\n' +
    '            <label>Password</label>\n' +
    '            <input type="password" ng-model="vm.password">\n' +
    '        </md-input-container>\n' +
    '\n' +
    '        <md-input-container class="ResetPassword-input">\n' +
    '            <label>Confirm Password</label>\n' +
    '            <input type="password" ng-model="vm.password_confirmation">\n' +
    '        </md-input-container>\n' +
    '\n' +
    '        <md-button type="submit" class="md-primary md-raised">Submit</md-button>\n' +
    '    </div>\n' +
    '\n' +
    '</form>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.partials');
} catch (e) {
  module = angular.module('app.partials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('./views/app/components/sidenav/sidenav.component.html',
    '<md-sidenav md-component-id="left" class="md-sidenav-left"  md-is-locked-open="$mdMedia(\'gt-md\')">\n' +
    '  <md-toolbar class="md-theme-indigo">\n' +
    '        <h1 class="md-toolbar-tools">Arifu Web Learning App</h1>\n' +
    '  </md-toolbar>\n' +
    '  <md-list>\n' +
    '      <md-list-item ui-sref="app.clients">\n' +
    '        <md-icon md-svg-icon="cached"></md-icon>\n' +
    '        <p>Clients</p>\n' +
    '      </md-list-item>\n' +
    '      <md-divider></md-divider>\n' +
    '      <md-list-item ui-sref="app.programs">\n' +
    '        <md-icon md-svg-icon="cached"></md-icon>\n' +
    '        <p>Programs</p>\n' +
    '      </md-list-item>\n' +
    '      <md-divider></md-divider>\n' +
    '      <md-list-item ui-sref="app.users">\n' +
    '        <md-icon md-svg-icon="cached"></md-icon>\n' +
    '        <p>Users</p>\n' +
    '      </md-list-item>\n' +
    '      <md-divider></md-divider>\n' +
    '      <md-list-item ui-sref="app.roles">\n' +
    '        <md-icon md-svg-icon="cached"></md-icon>\n' +
    '        <p>Roles</p>\n' +
    '      </md-list-item>\n' +
    '      <md-divider></md-divider>\n' +
    '      <md-list-item ui-sref="app.permissions">\n' +
    '        <md-icon md-svg-icon="cached"></md-icon>\n' +
    '        <p>Permissions</p>\n' +
    '      </md-list-item>\n' +
    '      <md-divider></md-divider>\n' +
    '</md-list>\n' +
    '\n' +
    '</md-sidenav>\n' +
    '\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.partials');
} catch (e) {
  module = angular.module('app.partials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('./views/app/pages/clients/clients.page.html',
    '<md-content class="Page-container">\n' +
    '	<h2>All Clients</h2>\n' +
    '	<clients></clients>\n' +
    '</md-content>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.partials');
} catch (e) {
  module = angular.module('app.partials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('./views/app/pages/create-client/create-client.page.html',
    '<md-content class="Page-container">\n' +
    '	<h2>Add a new Client</h2>\n' +
    '	<client-form></client-form>\n' +
    '</md-content>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.partials');
} catch (e) {
  module = angular.module('app.partials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('./views/app/pages/footer/footer.page.html',
    '<md-content class="Page-Container Footer iOS-hack" layout-align="center center">\n' +
    '\n' +
    '<div class="Footer-text">\n' +
    '	\n' +
    '	Design by Victor Makokha\n' +
    '</div>\n' +
    '\n' +
    '</md-content>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.partials');
} catch (e) {
  module = angular.module('app.partials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('./views/app/pages/forgot-password/forgot-password.page.html',
    '<md-content class="Page-container">\n' +
    '    <div class="ForgotPassword-formContainer" layout="column" layout-align="center center">\n' +
    '\n' +
    '        <h1 class="md-headline">Forgot your password?</h1>\n' +
    '\n' +
    '        <forgot-password></forgot-password>\n' +
    '\n' +
    '    </div>\n' +
    '</md-content>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.partials');
} catch (e) {
  module = angular.module('app.partials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('./views/app/pages/header/header.page.html',
    '<md-content class="Page-Container DemoHeader">\n' +
    '	<div layout="row">\n' +
    '		<div flex="90" flex-offset="5" class="DemoHeader-container">\n' +
    '			<div layout="row" layout-align="space-between">\n' +
    '				<a ui-sref="app.landing">\n' +
    '					<img src="img/icons/logo.png"  class="DemoHeader-logo"/>\n' +
    '				</a>\n' +
    '				<div layout="row" layout-align="center stretch">\n' +
    '					<a hide-xs class="DemoHeader-link md-subhead" href="#/login">Login</a>\n' +
    '					<a hide-xs class="DemoHeader-link md-subhead" href="#/register">Register</a>\n' +
    '				</div>\n' +
    '			</div>\n' +
    '		</div>\n' +
    '	</div>\n' +
    '</md-content>\n' +
    '<div class="DemoHeader-spacer"></div>\n' +
    '\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.partials');
} catch (e) {
  module = angular.module('app.partials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('./views/app/pages/landing/landing.page.html',
    '<landing></landing>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.partials');
} catch (e) {
  module = angular.module('app.partials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('./views/app/pages/login/login.page.html',
    '<md-content class="Page-container">\n' +
    '    <div class="Login-formContainer" layout="column" layout-align="center center">\n' +
    '\n' +
    '        <h1 class="md-headline">Log in to your account</h1>\n' +
    '\n' +
    '        <login-form></login-form>\n' +
    '\n' +
    '    </div>\n' +
    '</md-content>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.partials');
} catch (e) {
  module = angular.module('app.partials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('./views/app/pages/register/register.page.html',
    '<md-content class="Page-container">\n' +
    '	<div flex="80" flex-offset="10">\n' +
    '		<div class="Register-formContainer" layout="column" layout-align="center center">\n' +
    '			<h1 class="md-headline">Create an account</h1>\n' +
    '\n' +
    '			<register-form></register-form>\n' +
    '\n' +
    '		</div>\n' +
    '	</div>\n' +
    '</md-content>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.partials');
} catch (e) {
  module = angular.module('app.partials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('./views/app/pages/reset-password/reset-password.page.html',
    '<md-content class="Page-container">\n' +
    '    <div class="ResetPassword-formContainer" layout="column" layout-align="center center">\n' +
    '\n' +
    '        <h1 class="md-headline">Reset Password</h1>\n' +
    '\n' +
    '        <reset-password></reset-password>\n' +
    '\n' +
    '    </div>\n' +
    '</md-content>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.partials');
} catch (e) {
  module = angular.module('app.partials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('./views/app/pages/sidenav/sidenav.page.html',
    '<md-content class="Page-container">\n' +
    '	<sidenav></sidenav>\n' +
    '</md-content>\n' +
    '');
}]);
})();
